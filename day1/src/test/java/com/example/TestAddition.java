package com.example;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestAddition {

    public Addition addition;

    @Before
    public void setUp() throws Exception {
        addition = new Addition();
    }

    @Test
    public void computeSumTest() throws Exception {
        double result = addition.compute(4, 5);
        Assert.assertEquals(9.0, result, 0.00001);
    }
}
